setlocal cindent
setlocal cinoptions=s,l1,b1,(0,u0,w0,W0,m0
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal textwidth=79
setlocal fo-=ro fo+=cql
